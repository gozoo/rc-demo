package main

import (
	"unsafe"
	"golang.org/x/sys/windows"
	"syscall"
	"io/ioutil"
	"log"
)

func MAKEINTRESOURCE(id uintptr) *uint16 {
	return (*uint16)(unsafe.Pointer(id))
}

var (
	modkernel32 = windows.NewLazySystemDLL("kernel32.dll")
	procLoadResource = modkernel32.NewProc("LoadResource")
	procFindResource = modkernel32.NewProc("FindResourceW")
	procLockResource = modkernel32.NewProc("LockResource")
	procSizeofResource = modkernel32.NewProc("SizeofResource")
	RT_RCDATA = MAKEINTRESOURCE(10)

)


func LoadResource(hModule windows.Handle, hResInfo windows.Handle) windows.Handle {
	r1, _, _ := syscall.Syscall(procLoadResource.Addr(), 2, uintptr(hModule), uintptr(hResInfo), 0)
	return windows.Handle(r1)
}

func FindResource(hModule windows.Handle, lpName *uint16, lpType *uint16) windows.Handle {
	r1, _, _ := syscall.Syscall(procFindResource.Addr(), 3, uintptr(hModule), uintptr(unsafe.Pointer(lpName)), uintptr(unsafe.Pointer(lpType)))
	return windows.Handle(r1)
}

func LockResource(handle windows.Handle) unsafe.Pointer {
	r1, _, _ := syscall.Syscall(procLockResource.Addr(), 1, uintptr(handle), 0, 0)
	return unsafe.Pointer(r1)
}

func SizeofResource(hModule windows.Handle, hResInfo windows.Handle) int {
	r1, _, _ := syscall.Syscall(procSizeofResource.Addr(), 2, uintptr(hModule), uintptr(hResInfo), 0)
	return int(r1)
}

func main() {
	hsrc := FindResource(0, MAKEINTRESOURCE(101), RT_RCDATA)
	hdata := LoadResource(0, hsrc)
	size := SizeofResource(0, hsrc)
	pvoid := LockResource(hdata)
	buff := make([]byte, 0)
	var bytes = (*[1<<31]byte)(unsafe.Pointer(pvoid))[:size]
	buff = append(buff, bytes...)
	err := ioutil.WriteFile("test.zip", buff, 0644)
	if err != nil {
		log.Panic(err)
	}
}
