# About
The demo illustrates how to add application icon to `exe` file generated by Golang and embed resource files into `exe` file. The solution is only for Windows executable application.

### Install MinGW
`choco install mingw`

### Create `main.exe.manifest` file
```
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<assembly xmlns="urn:schemas-microsoft-com:asm.v1" manifestVersion="1.0">
<assemblyIdentity
    version="1.0.0.0"
    processorArchitecture="x86"
    name="controls"
    type="win32"
/>
<dependency>
    <dependentAssembly>
        <assemblyIdentity
            type="win32"
            name="Microsoft.Windows.Common-Controls"
            version="6.0.0.0"
            processorArchitecture="*"
            publicKeyToken="6595b64144ccf1df"
            language="*"
        />
    </dependentAssembly>
</dependency>
</assembly>

```

### Create `main.rc` file
```
100 ICON    "main.ico"
100 24      "main.exe.manifest"
101 RCDATA  "content.zip"
```
### Build
In git-bash windows perform the following command:   
`windres -o main-res.syso main.rc && go build -i`